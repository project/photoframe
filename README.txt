Drupal Photoframe module:
-------------------------
Author - Chris Shattuck (www.impliedbydesign.com)
License - GPL


Overview:
-------------------------
The Photoframe module uses jQuery to wrap an image-based frame 
around content after a page has been loaded. This allows you to add nice
visual elements without extra markup.


Installation and configuration:
-------------------------
- Download the Photoframe module and copy it into your 'modules'
directory. 
- Go to Administer >> Modules and enable the module.
- Enable one or more Photoframes by visiting 
Administter >> Site Configuration >> Photoframe and
selecting the checkboxes above the frames you would like to load.

Keep in mind that each Photoframe you load has it's own
css file, so it adds some extra overhead (so more than 5 might
be excessive, especially if you don't use css aggregation).


To use photoframes in your content:
-------------------------
In the administrative settings, you will see that the 'class' for each
photoframe is listed. Just add this class to the element you want to
wrap the frame around, and jQuery will do the rest! For example:

<div class="photoframe-quote">Quote here</div>.


My content is squished, what do I do?!
-------------------------
Be default, Photoframe assumes that you want to wrap around
fixed-width content. This means that it will wrap nicely around an
image, but will squish your text into an ugly one-word-per-line column.
You can fix this one of two ways: 

1) Add something of a fixed width to the content. This can be a <div>
with a fixed width, or a spacer image.
2) Add one of the following styles to the styles.css file in your 
template directory:

.photoframe-table { width:100%; }
This will cause all frames to be wide and expanding.

.photoframe-quote-table { width:100%}
Replace the 'photoframe-quote' with the style you want to cause to expand.
This will limit just that style to expand, while the others are fixed.

If you need to make one of a certain style wide, and another of the same
style fixed, then you need to do something like:

<div class="surrounding-div"><div class="photoframe-quote">Content</div></div>

And then add the following in your style.css file:

.surrounding-div .photoframe-quote-table { width:100%; }
 
 
It looks like the module is installed, but I don't see any frames?
-------------------------
Make sure that when you copy the photoframe directory to your module
directory, that you copy the folder that contains the many sub-folders.
Depending on what extracting program you use, you may extract to a folder
called 'photoframe' which contains another folder called 'photoframe', and
it's that second folder that contains the sub-folders like 'quote' and
'drop-shadow'. It's that second one that you want to copy into your modules
folder, otherwise the module will not display the frames.


Getting new photoframes:
-------------------------
- Download additional Photoframes at http://www.impliedbydeisgn.com/drupal-module-photoframes
- After downloading the zip file, unzip the file and copy it to your photoframe
module directory.
- You can put any Photoframe folders you are not using into the
'disabled' folder in the Photoframe module folder. That way your
administrative page will load faster.


Last updated:
------------
